#include <iostream>
#include <vector>
using namespace std;

class VillageGraph {
public:
  VillageGraph(int V) : N(V), villages(new vector<int>[V]) {}
  ~VillageGraph() { delete[] villages; }

  void addEdge(int u, int v) {
    villages[u].push_back(v);
    villages[v].push_back(u);
  }

  int groupCount() {
    bool visited[N];
    for (int i = 0; i < N; i++)
      visited[i] = false;

    int count = 0;
    for (int i = 0; i < N; i++) {
      if (!visited[i]) {
        count++;
        DFS(i, visited);
      }
    }

    return count;
  }

private:
  int N;
  vector<int> *villages;

  void DFS(int v, bool visited[]) {
    visited[v] = true;
    for (int n : villages[v]) {
      if (!visited[n]) {
        DFS(n, visited);
      }
    }
  }
};

int main() {
  int n, m, k;
  cin >> n >> m >> k;

  VillageGraph villages(n);
  for (int i = 0; i < m; i++) {
    int a, b;
    scanf("%d %d", &a, &b);
    villages.addEdge(a - 1, b - 1);
  }

  int count = villages.groupCount();
  if (count - k <= 1) {
    cout << 1 << endl;
  } else {
    cout << count - k << endl;
  }
}
