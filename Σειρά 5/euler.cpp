#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n, m;
  cin >> n >> m;
  vector<int> graph[n];
  for (int i = 0; i < m; i++) {
    int u, v;
    cin >> u >> v;
    graph[u].push_back(v);
    graph[v].push_back(u);
  }

  int begin = 0, end = 0;
  int connections = 0;

  for (int i = 0; i < n; i++) {
    if (graph[i].size() % 2 != 0) {
      connections++;
      if (connections > 2) {
        cout << "IMPOSSIBLE" << endl;
        return 0;
      } else if (begin == 0)
        begin = i;
      else
        end = i;
    }
  }

  if (end == 0) {
    if (begin == 0)
      cout << "CYCLE" << endl;
    else
      cout << "IMPOSSIBLE" << endl;
  } else if (begin > end)
    cout << "PATH " << end << " " << begin << endl;
  else
    cout << "PATH " << begin << " " << end << endl;
}
