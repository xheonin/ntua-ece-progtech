#include <iostream>
#include <vector>
using namespace std;

class Graph {
public:
  Graph(int V) : N(V), graph(new vector<int>[V]) {}
  ~Graph() { delete[] graph; }

  void addEdge(int u, int v) { graph[u].push_back(v); }

  bool cycle(vector<int> &path) const {
    bool visited[N];
    int recStack[N];
    int start = 0;
    for (int i = 0; i < N; i++) {
      visited[i] = false;
      recStack[i] = -1;
    }

    for (int i = 0; i < N; i++) {
      int count = 0;
      if (isCyclicUtil(i, visited, recStack, count, start)) {
        for (int j = start; j < count; j++)
          path.push_back(recStack[j]);
        return true;
      }
    }

    return false;
  }

private:
  int N;
  vector<int> *graph;

  bool isCyclicUtil(int v, bool visited[], int recStack[], int &count,
                    int &start) const {
    if (!visited[v]) {
      visited[v] = true;
      recStack[count] = v;
      count++;
    }

    for (auto u : graph[v]) {
      if (!visited[u] && isCyclicUtil(u, visited, recStack, count, start))
        return true;
      else {
        for (int j = 0; j < count; j++)
          if (recStack[j] == u) {
            start = j;
            return true;
          }
      }
    }

    recStack[v] = -1;
    count--;
    return false;
  }
};

#ifndef CONTEST
int main() {
  int V, E;
  cin >> V >> E;
  Graph g(V);
  for (int i = 0; i < E; i++) {
    int u, v;
    cin >> u >> v;
    g.addEdge(u, v);
  }
  vector<int> path;
  bool c = g.cycle(path);
  if (c) {
    cout << "CYCLE: ";
    for (unsigned i = 0; i < path.size(); i++)
      cout << path[i] << (i == path.size() - 1 ? "\n" : " ");
  } else {
    cout << "NO CYCLE" << endl;
  }
}
#endif
