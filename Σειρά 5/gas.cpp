#include <iostream>
#include <vector>
using namespace std;

typedef pair<int, int> Road;

class CityExplorer {
public:
  CityExplorer(int V) : N(V), roads(new vector<Road>[V]) {}
  ~CityExplorer() { delete[] roads; }

  void addRoad(int u, int v, int l) {
    roads[u].push_back(make_pair(v, l));
    roads[v].push_back(make_pair(u, l));
  }

  void setCarCapacity(int c) { carCapacity = c; }

  int refills(int a, int b, int c, vector<int> &path) {
    bool visited[N];
    for (int i = 0; i < N; i++)
      visited[i] = false;
    setCarCapacity(c);

    int refills = 0;
    if (DFS(a, b, visited, path, refills, 0)) {
      return refills;
    } else {
      return 0;
    }
  }

private:
  int N, carCapacity;
  vector<Road> *roads;

  bool DFS(int v, int dest, bool visited[], vector<int> &path, int &refills,
           int gas) {
    visited[v] = true;
    path.push_back(v);

    if (v == dest) {
      return true;
    }

    for (auto i : roads[v]) {
      const int neighbor = i.first;
      const int dist = i.second;

      if (dist <= carCapacity && !visited[neighbor]) {
        if (dist <= gas) {
          if (DFS(neighbor, dest, visited, path, refills, gas - dist)) {
            return true;
          }
        } else {
          if (DFS(neighbor, dest, visited, path, refills, carCapacity - dist)) {
            refills++;
            return true;
          }
        }
      }
    }
    path.pop_back();
    return false;
  }
};

int main() {
  int n, m;
  cin >> n >> m;

  CityExplorer expl(n);

  for (int i = 0; i < m; i++) {
    int u, v, l;
    scanf("%d %d %d", &u, &v, &l);
    expl.addRoad(u, v, l);
  }

  int q;
  cin >> q;
  for (int i = 0; i < q; i++) {
    int a, b, c;
    scanf("%d %d %d", &a, &b, &c);

    vector<int> path;
    int r = expl.refills(a, b, c, path);
    if (r) {
      cout << "POSSIBLE: " << r << " fill(s),";
      for (int town : path) {
        cout << " " << town;
      }
      cout << endl;
    } else {
      cout << "IMPOSSIBLE" << endl;
    }
  }
}
