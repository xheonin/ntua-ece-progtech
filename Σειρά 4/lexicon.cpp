#include <iostream>

using namespace std;

class lexicon {
public:
  lexicon() : root(NULL) {}

  ~lexicon() { destroy(root); }

  void insert(const string &s) { root = insertWord(root, s, 1); }

  int lookup(const string &s) const {
    int level = 0;
    word *search = searchWord(root, s, level);
    if (search == NULL)
      return 0;
    else
      return search->num;
  }

  int depth(const string &s) {
    int level = -1;
    word *search = searchWord(root, s, level);
    if (search == NULL)
      return -1;
    else
      return level;
  }

  void replace(const string &s1, const string &s2) {
    int level = 0;
    word *search1 = searchWord(root, s1, level);
    if (search1 == NULL)
      return;

    int num = search1->num;
    word *search2 = searchWord(root, s2, level);
    if (search2 == NULL)
      insertWord(root, s2, num);
    else
      search2->num += num;
    root = deleteWord(root, s1);
  }

  friend ostream &operator<<(ostream &out, const lexicon &l) {
    l.formatInorder(l.root, out);
    return out;
  }

private:
  struct word {
    int num;
    string data;
    word *left, *right;

    word(const string &data, int num)
        : num(num), data(data), left(NULL), right(NULL) {}
  };
  word *root;

  static word *insertWord(word *n, const string &s, const int num) {
    if (n == NULL)
      return new word(s, num);

    if (n->data == s)
      n->num += num;
    else if (n->data > s)
      n->left = insertWord(n->left, s, num);
    else
      n->right = insertWord(n->right, s, num);
    return n;
  }

  static word *searchWord(word *n, const string &s, int &level) {
    if (n == NULL)
      return NULL;

    level++;
    if (n->data == s)
      return n;
    if (n->data > s)
      return searchWord(n->left, s, level);
    else
      return searchWord(n->right, s, level);
  }

  static word *deleteWord(word *n, const string &s) {
    if (n == NULL)
      return n;

    if (n->data > s)
      n->left = deleteWord(n->left, s);
    else if (n->data < s)
      n->right = deleteWord(n->right, s);
    else {
      if ((n->left == NULL) && (n->right == NULL))
        return NULL;
      else if (n->left == NULL) {
        word *tmp = n->right;
        free(n);
        return tmp;
      } else if (n->right == NULL) {
        word *tmp = n->left;
        free(n);
        return tmp;
      }

      word *tmp = findMinimumWord(n);
      n->data = tmp->data;
      n->num = tmp->num;
      n->left = deleteWord(n->left, tmp->data);
    }

    return n;
  }

  static word *findMinimumWord(word *n) {
    word *tmp = n;
    while (tmp->left != NULL)
      tmp = tmp->left;
    return tmp;
  }

  static void formatInorder(word *n, ostream &out) {
    if (n != NULL) {
      formatInorder(n->left, out);
      out << n->data << " " << n->num << endl;
      formatInorder(n->right, out);
    }
  }

  static void destroy(word *n) {
    if (n != NULL) {
      destroy(n->left);
      destroy(n->right);
      delete n;
    }
  }
};

#ifndef CONTEST
int main() {
  // 1st test case
  lexicon l;
  l.insert("the");
  l.insert("boy");
  l.insert("and");
  l.insert("the");
  l.insert("wolf");
  cout << "and is now found " << l.lookup("and") << " time(s) at depth "
       << l.depth("and") << endl;
  cout << "boy is now found " << l.lookup("boy") << " time(s) at depth "
       << l.depth("boy") << endl;
  cout << "the is now found " << l.lookup("the") << " time(s) at depth "
       << l.depth("the") << endl;

  cout << "wolf is now found " << l.lookup("wolf") << " time(s) at depth "
       << l.depth("wolf") << endl;
  cout << "dummy is now found " << l.lookup("dummy") << " time(s) at depth "
       << l.depth("dummy") << endl;

  l.replace("and", "dummy");
  cout << "wolf is now found " << l.lookup("wolf") << " time(s) at depth "
       << l.depth("wolf") << endl;
  cout << "dummy is now found " << l.lookup("dummy") << " time(s) at depth "
       << l.depth("dummy") << endl;

  l.replace("boy", "dummy");
  cout << "wolf is now found " << l.lookup("wolf") << " time(s) at depth "
       << l.depth("wolf") << endl;
  cout << "dummy is now found " << l.lookup("dummy") << " time(s) at depth "
       << l.depth("dummy") << endl;

  l.replace("the", "dummy");
  cout << "wolf is now found " << l.lookup("wolf") << " time(s) at depth "
       << l.depth("wolf") << endl;
  cout << "dummy is now found " << l.lookup("dummy") << " time(s) at depth "
       << l.depth("dummy") << endl;

  l.replace("wolf", "dummy");
  cout << "wolf is now found " << l.lookup("wolf") << " time(s) at depth "
       << l.depth("wolf") << endl;
  cout << "dummy is now found " << l.lookup("dummy") << " time(s) at depth "
       << l.depth("dummy") << endl;

  cout << l << endl;
}
#endif
