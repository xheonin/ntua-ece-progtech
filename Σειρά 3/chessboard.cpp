#include <cmath>
#include <iomanip>
#include <iostream>
#include <stdexcept>
using namespace std;

class ChessBoardArray {
protected:
  int *data;
  unsigned size, base;
  class Row {
  public:
    Row(ChessBoardArray &a, int i) : board(a), row(i) {}
    int &operator[](int i) const { return board.select(row, i); }

  private:
    ChessBoardArray &board;
    int row;
  };

  class ConstRow {
  public:
    ConstRow(const ChessBoardArray &a, int i) : board(a), row(i) {}
    int operator[](int i) const { return board.select(row, i); }

  private:
    const ChessBoardArray &board;
    int row;
  };

public:
  ChessBoardArray(unsigned size = 0, unsigned base = 0)
      : data(new int[board_size(size)]), size(size), base(base) {
    for (unsigned i = 0; i < board_size(size); i++)
      data[i] = 0;
  }

  ChessBoardArray(const ChessBoardArray &a)
      : data(new int[board_size(a.size)]), size(a.size), base(a.base) {
    for (unsigned i = 0; i < board_size(size); i++)
      data[i] = a.data[i];
  }

  ~ChessBoardArray() { delete[] data; }

  ChessBoardArray &operator=(const ChessBoardArray &a) {
    delete[] data;
    size = a.size;
    base = a.base;
    data = new int[board_size(size)];
    for (unsigned i = 0; i < board_size(size); i++)
      data[i] = a.data[i];
    return *this;
  }

  int &select(int i, int j) { return data[loc(i, j)]; }

  int select(int i, int j) const { return data[loc(i, j)]; };

  const Row operator[](int i) { return Row(*this, i); }

  const ConstRow operator[](int i) const { return ConstRow(*this, i); }

  friend ostream &operator<<(ostream &out, const ChessBoardArray &a) {
    for (unsigned int i = a.base; i < a.size + a.base; i++) {
      for (unsigned int j = a.base; j < a.size + a.base; j++) {
        unsigned n;
        try {
          n = a[i][j];
        } catch (out_of_range) {
          n = 0;
        }
        out << setw(4) << n;
      }
      out << endl;
    }

    return out;
  }

private:
  unsigned int loc(int i, int j) const throw(out_of_range) {
    int di = i - base, dj = j - base;
    if (di < 0 || dj < 0 || di >= size || dj >= size)
      throw out_of_range("invalid block");

    if ((di % 2 == 0 && dj % 2 == 1) || (di % 2 == 1 && dj % 2 == 0))
      throw out_of_range("block is black");

    unsigned int n = di * size + dj;
    return n / 2;
  }

  unsigned int board_size(unsigned int i) { return round((i * i) / 2.0); }
};

#ifndef CONTEST
int main() {
  ChessBoardArray a(4, 1);
  a[3][1] = 42;
  a[4][4] = 17;
  try {
    a[2][1] = 7;
  } catch (out_of_range &e) {
    cout << "a[2][1] is black" << endl;
  }
  try {
    cout << a[1][2] << endl;
  } catch (out_of_range &e) {
    cout << "and so is a[1][2]" << endl;
  }
  cout << a;
}
#endif
