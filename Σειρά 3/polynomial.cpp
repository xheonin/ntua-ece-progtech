#include <cmath>
#include <cstddef>
#include <iostream>
#include <stdexcept>
using namespace std;

class Polynomial {
protected:
  class Term {
  public:
    int getExponent() { return exponent; }
    int getCoefficient() { return coefficient; }
    Term *getNext() { return next; }

  protected:
    int exponent;
    int coefficient;
    Term *next;
    Term(int exp, int coeff, Term *n)
        : exponent(exp), coefficient(coeff), next(n) {}
    friend class Polynomial;
  };
  Term *head;

public:
  Polynomial() : head(NULL) {}

  Polynomial(const Polynomial &p) {
    head = NULL;
    Term *tmp = p.head;
    while (tmp != NULL) {
      this->addTerm(tmp->exponent, tmp->coefficient);
      tmp = tmp->next;
    }
  }

  ~Polynomial() {}

  Polynomial &operator=(const Polynomial &p) {
    head = NULL;
    Term *tmp = p.head;
    while (tmp != NULL) {
      this->addTerm(tmp->exponent, tmp->coefficient);
      tmp = tmp->next;
    }
    return *this;
  }

  void addTerm(int expon, int coeff) {
    if (coeff == 0)
      return;
    if (head == NULL) {
      head = new Term(expon, coeff, NULL);
      return;
    }

    Term *tmp = head;
    while (tmp->next != NULL)
      if (tmp->next->exponent >= expon)
        tmp = tmp->next;
      else
        break;

    if (tmp->exponent == expon) {
      tmp->coefficient += coeff;
      if (tmp->coefficient == 0) {
        if (tmp == head) {
          head = tmp->next;
          delete tmp;
        } else {
          Term *tmp1 = head;
          while (tmp1->next != NULL && tmp1->next != tmp)
            tmp1 = tmp1->next;
          tmp1->next = tmp->next;
          delete tmp;
        }
      }
    } else if (tmp != head || tmp->exponent > expon) {
      Term *p = new Term(expon, coeff, tmp->next);
      tmp->next = p;
    } else {
      Term *p = new Term(expon, coeff, tmp);
      head = p;
    }
  }

  double evaluate(double x) {
    double result = 0.0;
    Term *tmp = head;
    while (tmp != NULL) {
      result += pow(x, tmp->exponent) * tmp->coefficient;
      tmp = tmp->next;
    }
    return result;
  }

  friend Polynomial operator+(const Polynomial &p, const Polynomial &q) {
    Polynomial n(p);
    Term *tmp = q.head;
    while (tmp != NULL) {
      n.addTerm(tmp->getExponent(), tmp->getCoefficient());
      tmp = tmp->getNext();
    }

    return n;
  }

  friend Polynomial operator*(const Polynomial &p, const Polynomial &q) {
    Polynomial n;
    Term *tmp1 = p.head;
    while (tmp1 != NULL) {
      Term *tmp2 = q.head;
      while (tmp2 != NULL) {
        n.addTerm(tmp1->getExponent() + tmp2->getExponent(),
                  tmp1->getCoefficient() * tmp2->getCoefficient());
        tmp2 = tmp2->getNext();
      }
      tmp1 = tmp1->getNext();
    }

    return n;
  }

  friend ostream &operator<<(ostream &out, const Polynomial &p) {
    if (p.head == NULL) {
      out << "0";
      return out;
    }

    Term *tmp = p.head;
    bool isFirst = true;
    while (tmp != NULL) {
      if (tmp->getCoefficient() < 0)
        out << "- ";
      if (isFirst) {
        if (abs(tmp->getCoefficient()) != 1 || tmp->getExponent() == 0)
          out << abs(tmp->getCoefficient());
        isFirst = false;
      } else {
        if (tmp->getCoefficient() > 0)
          out << "+ ";
        if (abs(tmp->getCoefficient()) != 1 || tmp->getExponent() == 0)
          out << abs(tmp->getCoefficient());
      }

      if (tmp->getExponent() != 0)
        out << "x";

      if (tmp->getExponent() != 1 && tmp->getExponent() != 0)
        out << "^" << tmp->getExponent();

      if (tmp->getNext() != NULL)
        out << " ";
      tmp = tmp->getNext();
    }

    return out;
  }
};

#ifndef CONTEST
int main() {
  Polynomial p;
  p.addTerm(2, 1);
  p.addTerm(1, -1);
  p.addTerm(2, -1);
  p.addTerm(1, 1);
  Polynomial q;
  q.addTerm(0, 5);
  q.addTerm(1, 6);
  Polynomial a(p + q), b(p * q);
  cout << "P(x) = " << p << endl;
  cout << "P(0) = " << p.evaluate(0) << endl;
  cout << "P(-1) = " << p.evaluate(-1) << endl;
  cout << "P(-25) = " << p.evaluate(-25) << endl;
  cout << "P(44) = " << p.evaluate(44) << endl;
  cout << "Q(x) = " << q << endl;
  cout << "Q(0) = " << q.evaluate(0) << endl;
  cout << "Q(-1) = " << q.evaluate(-1) << endl;
  cout << "Q(-25) = " << q.evaluate(-25) << endl;
  cout << "Q(44) = " << q.evaluate(44) << endl;
  cout << "(P+Q)(x) = " << a << endl;
  cout << "(P+Q)(0) = " << a.evaluate(0) << endl;
  cout << "(P+Q)(-1) = " << a.evaluate(-1) << endl;
  cout << "(P+Q)(-25) = " << a.evaluate(-25) << endl;
  cout << "(P+Q)(44) = " << a.evaluate(44) << endl;
  cout << "(P*Q)(x) = " << b << endl;
  cout << "(P*Q)(0) = " << b.evaluate(0) << endl;
  cout << "(P*Q)(-1) = " << b.evaluate(-1) << endl;
  cout << "(P*Q)(-25) = " << b.evaluate(-25) << endl;
  cout << "(P*Q)(44) = " << b.evaluate(44) << endl;
}
#endif
