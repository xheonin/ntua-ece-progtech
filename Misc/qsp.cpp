#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
using namespace std;

int main() {
  int n;
  cin >> n;

  queue<int> q;
  stack<int> s;
  string instr;
  for (int i = 0; i < n; i++) {
    int in;
    cin >> in;
    q.push(in);
  }
  cin >> instr;

  for (int i = 0; i < instr.length(); i++) {
    char e = instr[i];
    switch (e) {
    case 'Q':
      if (q.empty()) {
        cout << "error" << endl;
        exit(0);
      }

      s.push(q.front());
      q.pop();
      break;
    case 'S':
      if (s.empty()) {
        cout << "error" << endl;
        exit(0);
      }

      q.push(s.top());
      s.pop();
      break;
    case 'P':
      queue<int> temp = q;
      while (!temp.empty()) {
        cout << temp.front();
        if (temp.front() != temp.back())
          cout << " ";
        temp.pop();
      }
      cout << endl;
      break;
    }
  }
}
