#include <algorithm>
#include <iostream>
#include <set>
using namespace std;

int main() {
  int n, d;
  cin >> n >> d;
  set<string> l[d];
  for (int i = 0; i < n; i++) {
    int ln;
    string w;
    cin >> ln >> w;
    l[ln - 1].insert(w);
  }

  unsigned long maxl = 0;
  int maxn = 0;
  for (int i = 0; i < d; i++) {
    unsigned long size = l[i].size();
    if (maxl < size) {
      maxn = i;
      maxl = size;
    }
  }
  cout << "largest dictionary is " << maxn + 1 << " with " << maxl << " word(s)"
       << endl;

  set<string> shared;
  if (d == 1)
    shared = l[0];
  else {
    for (auto s = l[0].begin(); s != l[0].end(); s++) {
      if (l[1].find(*s) != l[1].end()) {
        shared.insert(*s);
      }
    }
    if (d > 2) {
      int rem = d - 2;
      for (int i = 2; i < rem + 2; i++) {
        for (auto s = shared.begin(); s != shared.end(); s++) {
          if (l[i].find(*s) == l[i].end()) {
            shared.erase(s);
          }
        }
      }
    }
  }

  for (auto s = shared.begin(); s != shared.end(); s++)
    cout << *s << endl;
  cout << shared.size() << " word(s) appear in all dictionaries" << endl;
}
