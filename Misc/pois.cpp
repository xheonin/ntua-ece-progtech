#include <cmath>
#include <float.h>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <tuple>
#include <vector>
using namespace std;

typedef tuple<double, double> Coords;

double distance(double x, double y) { return sqrt(pow(x, 2) + pow(y, 2)); }

int main() {
  int n;
  cin >> n;

  map<string, set<Coords>> pois;
  for (int i = 0; i < n; i++) {
    double x, y;
    string categ;
    cin >> x >> y >> categ;
    pois[categ].insert(Coords(x, y));
  }

  int k;
  cin >> k;

  vector<Coords> locs;
  for (int i = 0; i < k; i++) {
    double x, y;
    cin >> x >> y;
    locs.push_back(Coords(x, y));
  }

  for (auto &l : locs) {
    double x = get<0>(l), y = get<1>(l);
    cout << fixed << setprecision(0) << x << " " << y << endl;

    for (auto &p : pois) {
      cout << "  " << p.first << " ";
      double mindist = DBL_MAX;
      for (auto &c : p.second)
        mindist =
            min(mindist, distance(abs(x - get<0>(c)), abs(y - get<1>(c))));
      cout << fixed << setprecision(3) << mindist << endl;
    }
  }
}
