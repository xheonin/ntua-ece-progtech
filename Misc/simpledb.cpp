#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

struct Employee {
  unsigned id, year, salary;
  string firstName, lastName;

  friend ostream &operator<<(ostream &out, const Employee &e) {
    out << e.id << " " << e.firstName << " " << e.lastName << " " << e.year
        << " " << e.salary;
    return out;
  }
};

bool compareId(const Employee &e1, const Employee &e2) {
  return (e1.id < e2.id);
}

bool compareIdDesc(const Employee &e1, const Employee &e2) {
  return (e1.id > e2.id);
}

bool compareYear(const Employee &e1, const Employee &e2) {
  if (e1.year == e2.year)
    return compareId(e1, e2);
  return (e1.year < e2.year);
}

bool compareYearDesc(const Employee &e1, const Employee &e2) {
  if (e1.year == e2.year)
    return compareId(e1, e2);
  return (e1.year > e2.year);
}

bool compareSalary(const Employee &e1, const Employee &e2) {
  if (e1.salary == e2.salary)
    return compareId(e1, e2);
  return (e1.salary < e2.salary);
}

bool compareSalaryDesc(const Employee &e1, const Employee &e2) {
  if (e1.salary == e2.salary)
    return compareId(e1, e2);
  return (e1.salary > e2.salary);
}

bool compareName(const Employee &e1, const Employee &e2) {
  string name1 = e1.lastName + e1.firstName;
  string name2 = e2.lastName + e2.firstName;
  if (name1 == name2)
    return compareId(e1, e2);
  return name1 < name2;
}

bool compareNameDesc(const Employee &e1, const Employee &e2) {
  if (e1.salary == e2.salary)
    return compareId(e1, e2);
  return e1.lastName + e1.firstName > e2.lastName + e2.firstName;
}

struct Query {
  string what, order;
  unsigned items;

  friend ostream &operator<<(ostream &out, const Query &q) {
    out << "Query: " << q.what << " " << q.order << " " << q.items;
    return out;
  }
};

int main() {
  int n;
  cin >> n;

  vector<Employee> es;
  for (int i = 0; i < n; i++) {
    unsigned id, year, salary;
    string firstName, lastName;
    cin >> id >> firstName >> lastName >> year >> salary;
    es.push_back(Employee{id, year, salary, firstName, lastName});
  }

  int m;
  cin >> m;

  vector<Query> qs;
  for (int i = 0; i < m; i++) {
    string what, order;
    unsigned items;
    cin >> what >> order >> items;
    qs.push_back(Query{what, order, items});
  }

  for (auto &q : qs) {
    cout << q << endl;

    vector<Employee> sorted = es;
    if (q.order == "asc") {
      if (q.what == "id")
        sort(sorted.begin(), sorted.end(), compareId);
      else if (q.what == "name")
        sort(sorted.begin(), sorted.end(), compareName);
      else if (q.what == "year")
        sort(sorted.begin(), sorted.end(), compareYear);
      else if (q.what == "salary")
        sort(sorted.begin(), sorted.end(), compareSalary);
    }
    else {
      if (q.what == "id")
        sort(sorted.begin(), sorted.end(), compareIdDesc);
      else if (q.what == "name")
        sort(sorted.begin(), sorted.end(), compareNameDesc);
      else if (q.what == "year")
        sort(sorted.begin(), sorted.end(), compareYearDesc);
      else if (q.what == "salary")
        sort(sorted.begin(), sorted.end(), compareSalaryDesc);
    }

    for (int i = 0; i < q.items; i++)
      cout << sorted[i] << endl;
  }
}
