#include <climits>
#include <iostream>
#include <map>
#include <set>
using namespace std;

struct LetterInfo {
  unsigned long words, uniqueWords, longestLen, shortestLen;
  string longest, shortest;
};

int main() {
  multiset<string> txt;
  string tmp;
  while (cin >> tmp) {
    txt.insert(tmp);
  }

  map<char, LetterInfo> stats;
  for (auto word = txt.begin(); word != txt.end(); word++) {
    string w = *word;
    char l = w[0];

    LetterInfo info;

    try {
      auto s = stats.at(l);
      info = s;
    } catch (const exception &e) {
      info = LetterInfo{0, 0, 0, ULONG_MAX, "", ""};
    }

    unsigned long count = txt.count(w);
    info.words += count;
    info.uniqueWords++;
    if (count > 1)
      txt.erase(word++);

    unsigned long len = w.length();
    if (len > info.longestLen) {
      info.longestLen = len;
      info.longest = w;
    }

    if (len < info.shortestLen) {
      info.shortestLen = len;
      info.shortest = w;
    }

    stats[l] = info;
  }

  for (auto i = stats.begin(); i != stats.end(); i++) {
    auto item = *i;
    cout << item.first << ": " << item.second.words << " word(s), "
         << item.second.uniqueWords << " unique word(s), longest '"
         << item.second.longest << "' and shortest '" << item.second.shortest
         << "'" << endl;
  }
}
