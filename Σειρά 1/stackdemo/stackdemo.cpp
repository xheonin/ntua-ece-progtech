#include <algorithm>
#include <iostream>
using namespace std;

template <typename T> class stack {
public:
  stack(int size) {
    last = 0;
    len = size;
    array = new T[size];
  }

  stack(const stack &s) {
    last = s.last;
    len = s.len;
    array = new T[len];
    copy(s.array, s.array + s.last, array);
  }

  ~stack() { delete[] array; }

  const stack &operator=(const stack &s) {
    last = s.last;
    len = s.len;
    delete[] array;
    array = new T[len];
    copy(s.array, s.array + s.last, array);
    return s;
  }

  friend ostream &operator<<(ostream &out, const stack &s) {
    out << "[";

    for (int i = 0; i < s.last; i++) {
      out << s.array[i];
      if (i != s.last - 1) {
        out << ", ";
      }
    }

    return out << "]";
  }

  bool empty() { return last == 0; }

  void push(const T &x) { array[last++] = x; }

  T pop() { return array[--last]; }

  int size() { return last; }

private:
  int last, len;
  T *array;
};

#ifndef CONTEST
int main() {
  stack<int> s(10);
  cout << "s is empty: " << s << endl;
  s.push(42);
  cout << "s has one element: " << s << endl;
  s.push(17);
  s.push(34);
  cout << "s has more elements: " << s << endl;
  cout << "How many? " << s.size() << endl;
  stack<int> t(5);
  t.push(7);
  cout << "t: " << t << endl;
  t = s;
  cout << "popping from s: " << s.pop() << endl;

  s.push(8);
  stack<int> a(s);
  t.push(99);
  a.push(77);
  cout << "s: " << s << endl;
  cout << "t: " << t << endl;
  cout << "a: " << a << endl;

  stack<double> c(4);
  c.push(3.14);
  c.push(1.414);
  cout << "c contains doubles " << c << endl;

  stack<char> k(4);
  k.push('$');
  cout << "k contains a character " << k << endl;
}
#endif
