 #include <iostream>
using namespace std;

#ifndef CONTEST
#include "fullratio.hpp"
#endif

rational::rational(int n, int d) {
  nom = n;
  den = d;
}

int rational::gcd(int a, int b) {
  if (b == 0)
    return a;
  return gcd(b, a % b);
}

rational operator+(const rational &x, const rational &y) {
  int n = (x.nom * y.den) + (y.nom * x.den);
  int d = x.den * y.den;

  return rational(n, d);
}

rational operator-(const rational &x, const rational &y) {
  int n = (x.nom * y.den) - (y.nom * x.den);
  int d = x.den * y.den;

  return rational(n, d);
}

rational operator*(const rational &x, const rational &y) {
  int n = x.nom * y.nom, d = x.den * y.den;

  return rational(n, d);
}

rational operator/(const rational &x, const rational &y) {
  int n = x.nom * y.den, d = x.den * y.nom;

  return rational(n, d);
}

std::ostream &operator<<(std::ostream &out, const rational &x) {
  if (!((x.nom <= 0 && x.den <= 0) || (x.nom >= 0 && x.den >= 0)))
    out << '-';

  int gcd = rational::gcd(x.nom, x.den);
  return out << abs(x.nom / gcd) << "/" << abs(x.den / gcd);
}
