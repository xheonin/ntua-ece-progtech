#include <iostream>
using namespace std;

#ifndef CONTEST
#include "babyratio.hpp"
#endif

rational::rational(int n, int d) {
  nom = n;
  den = d;
}

int rational::gcd(int a, int b) {
  if (b == 0)
    return a;
  return gcd(b, a % b);
}

rational rational::add(rational r) {
  int n = (nom * r.den) + (r.nom * den);
  int d = den * r.den;

  return rational(n, d);
}

rational rational::sub(rational r) {
  int n = (nom * r.den) - (r.nom * den);
  int d = den * r.den;

  return rational(n, d);
}

rational rational::mul(rational r) {
  int n = r.nom * nom, d = r.den * den;

  return rational(n, d);
}

rational rational::div(rational r) {
  int n = r.den * nom, d = r.nom * den;

  return rational(n, d);
}

void rational::print() {
  if (!((nom <= 0 && den <= 0) || (nom >= 0 && den >= 0)))
    cout << '-';

  int gcd = rational::gcd(nom, den);
  cout << abs(nom / gcd) << "/" << abs(den / gcd);
}
